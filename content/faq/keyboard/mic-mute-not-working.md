+++
title = "The microphone mute button doesn't work"
+++

The issue is that the current keycode being emitted by the driver is only recognised by Wayland. X11 and desktops using X11 require `F20` to be emitted.

Create a file named `/etc/udev/hwdb.d/90-nkey.hwdb` with:

```
# Format evdev:input:b<bus_id>v<vendor_id>p<product_id>

# ** Note **
# The line evdev:input:b0003v0B05p1866* may vary on your ASUS Laptop.  
# Modify the <vendor_id> and <product_id> based on the output of this command to ensure remaps work:
# $ lsusb | grep 'ASUSTek Computer, Inc. N-KEY Device' | awk -F'[: ]' '{print $7" "$8}' | tr '[:lower:]' '[:upper:]'

evdev:input:b0003v0B05p1866*
  KEYBOARD_KEY_ff31007c=f20 # x11 mic-mute
```

then update hwdb with:

```
sudo systemd-hwdb update
sudo udevadm trigger
```