+++
title = "Can I re-map the arrow keys?"
+++

Yes, create a file named `/etc/udev/hwdb.d/90-nkey.hwdb` with:

```
# Format evdev:input:b<bus_id>v<vendor_id>p<product_id>

# ** Note **
# The line evdev:input:b0003v0B05p1866* may vary on your ASUS Laptop.  
# Modify the <vendor_id> and <product_id> based on the output of this command to ensure remaps work:
# $ lsusb | grep 'ASUSTek Computer, Inc. N-KEY Device' | awk -F'[: ]' '{print $7" "$8}' | tr '[:lower:]' '[:upper:]'

evdev:input:b0003v0B05p1866*
  KEYBOARD_KEY_c00b6=kbdillumdown # Fn+F2 (music prev)
  KEYBOARD_KEY_c00b5=kbdillumup   # Fn+F4 (music skip)
  KEYBOARD_KEY_ff3100c5=pagedown  # Fn+Down
  KEYBOARD_KEY_ff3100c4=pageup    # Fn+Up
  KEYBOARD_KEY_ff3100b2=home      # Fn+Left
  KEYBOARD_KEY_ff3100b3=end       # Fn+Right
```

then update hwdb with:

```
sudo systemd-hwdb update
sudo udevadm trigger
```

You can see a list of keycodes [here](https://github.com/torvalds/linux/blob/b76f733c3ff83089cf1e3f9ae233533649f999b3/include/uapi/linux/input-event-codes.h).
