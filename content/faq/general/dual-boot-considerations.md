+++
title = "What steps are needed if I want to dual boot?"
+++

Be sure to consider the following:
- disable fast boot within the BIOS
- disable fast boot in Windows
- always fully shutdown after using and switchting to another OS so the hardware gets correctly initialized

If you still experience an issue, hold down the power button while on battery for a few seconds to force a shutdown. This often helps to reset some things.

These steps are not needed if you are running Linux exclusively.