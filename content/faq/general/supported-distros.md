+++
title = "Is `<distro>` supported by asusctl?"
+++

Only Fedora (current version) and OpenSUSE Tumbleweed officially. Other distros may have
packaging, or you will need to compile and install it yourself.